package slurm

import (
	"bytes"
	"errors"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
//	"path/filepath"
	"math/rand"
	"bufio"
	"fmt"
	"strings"
	"time"
	"strconv"
	"gitlab.com/nickaj/gitlab-ci-multi-runner/shells"
	"gitlab.com/nickaj/gitlab-ci-multi-runner/common"
	"gitlab.com/nickaj/gitlab-ci-multi-runner/executors"
	"gitlab.com/nickaj/gitlab-ci-multi-runner/helpers"

)


type SlurmExecutor struct {
	executors.AbstractExecutor
	cmd       *exec.Cmd
	scriptDir string
	pscript   string
	bld		 *common.Build
}

func (s *SlurmExecutor) Prepare(globalConfig *common.Config, config *common.RunnerConfig, build *common.Build) error {
	if globalConfig != nil {
		s.Shell.User = globalConfig.User
	}

	err := s.AbstractExecutor.Prepare(globalConfig, config, build)
	if err != nil {
		return err
	}



	s.Println("Using slurm executor...")
	s.pscript = helpers.ShellEscape(build.FullProjectDir() + ".sh")
	s.bld = build
	return nil
}

func (s *SlurmExecutor) Start() error {
	s.Debugln("Starting slurm command...")

	// Create execution command
	s.cmd = exec.Command(s.ShellScript.Command, s.ShellScript.Arguments...)
	if s.cmd == nil {
		return errors.New("Failed to generate execution command")
	}

	helpers.SetProcessGroup(s.cmd)

	// Fill process environment variables
	s.cmd.Env = append(os.Environ(), s.ShellScript.Environment...)
	s.cmd.Stderr = s.BuildLog
	
	// Generate a random filename for the file to be passed to SLURM.
	// Not doing this could cause a collision in the case of concurrent runs.
	rand.Seed(time.Now().UTC().UnixNano())
	slurm_file := strconv.Itoa(rand.Int())
	slurm_file = slurm_file + ".slurm"
	
	Sstdout,_ := s.cmd.StdoutPipe()
	s.cmd.Stdin = nil
	s.cmd.Args = append(s.cmd.Args, "--command", "sbatch " + slurm_file)
	
	// Create a file to hold the SLURM script which gets dispatched to the back-end node
	// f, err := os.Create("generated_slurm.slurm")
	f, err := os.Create(slurm_file)
	if err != nil {
		return errors.New("Failed to create SLURM script file.")
	}
	defer f.Close()
	
	projectDir := s.bld.FullProjectDir()
	projectDir = helpers.ToSlash(projectDir)
	
	w := bufio.NewWriter(f)
	
	commands := s.bld.Commands
	commands = strings.TrimSpace(commands)
	svars, scmds := s.seperateCommands(commands)		
	
	// Generic SLURMy HEADER	
	io.WriteString(w, fmt.Sprintf("#!/bin/bash\n"))	
	io.WriteString(w, svars.String())
		
	bv := s.bld.Variables
	var abs shells.AbstractShell
	for _, keyValue := range abs.GetVariables(s.bld, projectDir, bv) {
		io.WriteString(w, "export " + helpers.ShellEscape(keyValue) + "\n")
	}
		
	io.WriteString(w, fmt.Sprintf("rm -rf %s\n", projectDir))
	io.WriteString(w, fmt.Sprintf("mkdir -p %s\n", projectDir))
	io.WriteString(w, fmt.Sprintf("git clone %s %s\n", helpers.ShellEscape(s.bld.RepoURL), projectDir))
	io.WriteString(w, fmt.Sprintf("cd %s\n", projectDir))
	io.WriteString(w, scmds.String())
	
	w.Flush()
	
	err = s.cmd.Start()
	if err != nil {
		return errors.New("Failed to start process")
	}
	
	// Process jobid
	t1,_ := ioutil.ReadAll(Sstdout)
	jobid := strings.Split(string(t1), " ")[3]
	jobid = strings.TrimSuffix(jobid, "\n")
	s.cmd.Wait() // Wait for launch command to complete. Should return without blocking
	s.Println("Job ID " + jobid + " has been passed to the scheduler for execution & is waiting for completion.")
	
	// This must be the last thing in the code as sending *anything* down the s.BuildFinish channel indicates a completed build
	// We need to use this to indicate that the SLURM job has completed.	
	go s.waitCompleted(jobid)

	return nil
}

func (s *SlurmExecutor) Cleanup() {
	helpers.KillProcessGroup(s.cmd)

	if s.scriptDir != "" {
		os.RemoveAll(s.scriptDir)
	}

	s.AbstractExecutor.Cleanup()
}

func init() {
	options := executors.ExecutorOptions{
		DefaultBuildsDir: "builds",
		SharedBuildsDir:  true,
		Shell: common.ShellScriptInfo{
			Shell: common.GetDefaultShell(),
			Type:  common.LoginShell,
		},
		ShowHostname: false,
	}

	create := func() common.Executor {
		return &SlurmExecutor{
			AbstractExecutor: executors.AbstractExecutor{
				ExecutorOptions: options,
			},
		}
	}

	common.RegisterExecutor("slurm", common.ExecutorFactory{
		Create: create,
		Features: common.FeaturesInfo{
			Variables: true,
		},
	})
}

func assemblevars(wr *bufio.Writer){
	wr.WriteString("hostname\n")
}

// Launch an instance of sacct to look for the job's status by job id
// Relies on the output string of sacct containing "COMPLETED" which could change
// It would be much better if SLURM had a Go binding + API calls
func (s *SlurmExecutor) checkCompleted(jobid string) bool {
	rCmd := exec.Command("su", "--shell", "/bin/bash", "--login", "runner", "--command", "/usr/bin/sacct -j " + jobid)
	helpers.SetProcessGroup(rCmd)
	rCmd.Env = append(os.Environ(), s.ShellScript.Environment...)
	rCmd.Stderr = s.BuildLog
	rCmd.Stdin = nil
	rOut,_ := rCmd.Output()    
    return strings.Contains(string(rOut), "COMPLETED")
}


// Loop over calls to checkCompleted to see if the job has finished
// It is somewhat wasteful to keep launching a shell each time, but convenient
// Sending 'nil' to s.BuildFinish tells the rest of the code that this job is complete
func (s *SlurmExecutor) waitCompleted(jobid string){
	var enderr error
	enderr = nil
	for {
		c := s.checkCompleted(jobid)
		if c == true {
			s.Infoln("Job output follows:")
			s.getSLURMoutput(jobid)
			s.BuildFinish <- enderr
			break
		}
		time.Sleep(5*time.Second)
	}	
}

func (s *SlurmExecutor) getSLURMoutput(jobid string){
	
	rCmd := exec.Command("su", "--shell", "/bin/bash", "--login", "runner", "--command", "cat ~/slurm-" + jobid +".out")
	helpers.SetProcessGroup(rCmd)
	rCmd.Env = append(os.Environ(), s.ShellScript.Environment...)
	rCmd.Stdout = s.BuildLog
	rCmd.Stdin = nil
	rCmd.Start()
	rCmd.Wait()	
		
}

func(s *SlurmExecutor) seperateCommands(orig_cmds string) (bytes.Buffer, bytes.Buffer){
	
	ntasks := "1"
	jobname := "slurmjob"
	partition := "general"
	nodelist := "fermi0"
	
	var slurmvars bytes.Buffer
	var slurmcmds bytes.Buffer
	
	for _, command := range strings.Split(orig_cmds, "\n") {
	command = strings.TrimSuffix(command, "\r") // CI uses windows linebreaks for the commands, strip if they exist otherwise SLURM fails	
		if cs := strings.Split(command, ":"); cs[0] == "NODELIST" {
			nodelist = strings.TrimSuffix(cs[1], "\n")
			slurmvars.WriteString(fmt.Sprintf("#SBATCH --nodelist=%s\n", nodelist))
		} else if cs := strings.Split(command, ":"); cs[0] == "PARTITION" {
			partition = strings.TrimSuffix(cs[1], "\n")
			slurmvars.WriteString(fmt.Sprintf("#SBATCH --partition=%s\n", partition))		
		} else if cs := strings.Split(command, ":"); cs[0] == "NTASKS" {
			ntasks = strings.TrimSuffix(cs[1], "\n")
			slurmvars.WriteString(fmt.Sprintf("#SBATCH --ntasks=%s\n", ntasks))
		} else if cs := strings.Split(command, ":"); cs[0] == "JOBNAME" {
			jobname = strings.TrimSuffix(cs[1], "\n")
			slurmvars.WriteString(fmt.Sprintf("#SBATCH --job-name=%s\n", jobname))
		} else {
			slurmcmds.WriteString(command+"\n")
		}
	}
		
	return slurmvars, slurmcmds
}